# Word Embeddings

Word embeddings can be downloaded from their respective Web sites and repositories.

## Word2vec

- _url_: https://code.google.com/archive/p/word2vec/

## GloVe

- _url_: https://nlp.stanford.edu/projects/glove/

## fastText

- _url_: https://fasttext.cc/

