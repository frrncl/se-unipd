/*
 *  Copyright 2017-2022 University of Padua, Italy
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package it.unipd.dei.se.index;

import it.unipd.dei.se.parse.DocumentParser;
import it.unipd.dei.se.parse.ParsedDocument;
import it.unipd.dei.se.parse.TipsterParser;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.core.LowerCaseFilterFactory;
import org.apache.lucene.analysis.custom.CustomAnalyzer;
import org.apache.lucene.analysis.standard.StandardTokenizerFactory;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;
import java.util.Map;

/**
 * Indexes documents processing a whole directory tree.
 *
 * @author Nicola Ferro
 * @version 1.00
 * @since 1.00
 */
public class DirectoryIndexer {

	/**
	 * One megabyte
	 */
	private static final int MBYTE = 1024 * 1024;

	/**
	 * The class of the {@code DocumentParser} to be used.
	 */
	private final Class<? extends DocumentParser> dpCls;

	/**
	 * The directory (and sub-directories) where documents are stored.
	 */
	private final Path docsDir;

	/**
	 * The extension of the files to be indexed.
	 */
	private final String extension;

	/**
	 * The charset used for encoding documents.
	 */
	private final Charset cs;

	/**
	 * The total number of documents expected to be indexed.
	 */
	private final long expectedDocs;

	/**
	 * The start instant of the indexing.
	 */
	private final long start;

	/**
	 * The total number of indexed files.
	 */
	private long filesCount;

	/**
	 * The total number of indexed documents.
	 */
	private long docsCount;

	/**
	 * The total number of indexed bytes
	 */
	private long bytesCount;

	/**
	 * The directory where to store the index.
	 */
	private final Path indexDir;

	/**
	 * The configuration for the {@code IndexWriter}.
	 */
	final IndexWriterConfig iwc;

	/**
	 * The file where to write the vocabulary
	 */
	private final Path vocabularyPath;

	/**
	 * Creates a new indexer.
	 *
	 * @param analyzer        the {@code Analyzer} to be used.
	 * @param ramBufferSizeMB the size in megabytes of the RAM buffer for indexing documents.
	 * @param indexPath       the directory where to store the index.
	 * @param docsPath        the directory from which documents have to be read.
	 * @param extension       the extension of the files to be indexed.
	 * @param charsetName     the name of the charset used for encoding documents.
	 * @param expectedDocs    the total number of documents expected to be indexed
	 * @param dpCls           the class of the {@code DocumentParser} to be used.
	 * @param vocabularyFile  the file where to write the vocabulary
	 *
	 * @throws NullPointerException     if any of the parameters is {@code null}.
	 * @throws IllegalArgumentException if any of the parameters assumes invalid values.
	 */
	public DirectoryIndexer(final Analyzer analyzer, final int ramBufferSizeMB,
							final String indexPath, final String docsPath, final String extension,
							final String charsetName, final long expectedDocs,
							final Class<? extends DocumentParser> dpCls, final String vocabularyFile) {

		if (dpCls == null) {
			throw new NullPointerException("Document parser class cannot be null.");
		}

		this.dpCls = dpCls;

		if (analyzer == null) {
			throw new NullPointerException("Analyzer cannot be null.");
		}

		if (ramBufferSizeMB <= 0) {
			throw new IllegalArgumentException("RAM buffer size cannot be less than or equal to zero.");
		}

		iwc = new IndexWriterConfig(analyzer);
		iwc.setSimilarity(new BM25Similarity());
		iwc.setRAMBufferSizeMB(ramBufferSizeMB);
		iwc.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		iwc.setCommitOnClose(true);
		iwc.setUseCompoundFile(true);

		if (indexPath == null) {
			throw new NullPointerException("Index path cannot be null.");
		}

		if (indexPath.isEmpty()) {
			throw new IllegalArgumentException("Index path cannot be empty.");
		}

		indexDir = Paths.get(indexPath);

		// if the directory does not already exist, create it
		if (Files.notExists(indexDir)) {
			try {
				Files.createDirectory(indexDir);
			} catch (Exception e) {
				throw new IllegalArgumentException(
						String.format("Unable to create directory %s: %s.", indexDir.toAbsolutePath().toString(),
								e.getMessage()), e);
			}
		}

		if (!Files.isWritable(indexDir)) {
			throw new IllegalArgumentException(
					String.format("Index directory %s cannot be written.", indexDir.toAbsolutePath().toString()));
		}

		if (!Files.isDirectory(indexDir)) {
			throw new IllegalArgumentException(String.format("%s expected to be a directory where to write the index.",
					indexDir.toAbsolutePath().toString()));
		}

		if (docsPath == null) {
			throw new NullPointerException("Documents path cannot be null.");
		}

		if (docsPath.isEmpty()) {
			throw new IllegalArgumentException("Documents path cannot be empty.");
		}

		docsDir = Paths.get(docsPath);
		if (!Files.isReadable(docsDir)) {
			throw new IllegalArgumentException(
					String.format("Documents directory %s cannot be read.", docsDir.toAbsolutePath().toString()));
		}

		if (!Files.isDirectory(docsDir)) {
			throw new IllegalArgumentException(
					String.format("%s expected to be a directory of documents.", docsDir.toAbsolutePath().toString()));
		}

		vocabularyPath = Paths.get(vocabularyFile);
		if (Files.isDirectory(vocabularyPath)) {
			throw new IllegalArgumentException(
					String.format("%s expected to be a file where to write the vocabulary.", vocabularyPath.toAbsolutePath().toString()));
		}

		if (extension == null) {
			throw new NullPointerException("File extension cannot be null.");
		}

		if (extension.isEmpty()) {
			throw new IllegalArgumentException("File extension cannot be empty.");
		}
		this.extension = extension;

		if (charsetName == null) {
			throw new NullPointerException("Charset name cannot be null.");
		}

		if (charsetName.isEmpty()) {
			throw new IllegalArgumentException("Charset name cannot be empty.");
		}

		try {
			cs = Charset.forName(charsetName);
		} catch (Exception e) {
			throw new IllegalArgumentException(
					String.format("Unable to create the charset %s: %s.", charsetName, e.getMessage()), e);
		}

		if (expectedDocs <= 0) {
			throw new IllegalArgumentException(
					"The expected number of documents to be indexed cannot be less than or equal to zero.");
		}
		this.expectedDocs = expectedDocs;

		this.docsCount = 0;

		this.bytesCount = 0;

		this.filesCount = 0;

		this.start = System.currentTimeMillis();

	}

	/**
	 * Indexes the documents.
	 *
	 * @throws IOException if something goes wrong while indexing.
	 */
	public void index() throws IOException {

		System.out.printf("%n#### Start indexing ####%n");

		final IndexWriter writer = new IndexWriter(FSDirectory.open(indexDir), iwc);

		Files.walkFileTree(docsDir, new SimpleFileVisitor<>() {
			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				if (file.getFileName().toString().endsWith(extension)) {

					DocumentParser dp = DocumentParser.create(dpCls, Files.newBufferedReader(file, cs));

					bytesCount += Files.size(file);

					filesCount += 1;

					Document doc = null;

					for (ParsedDocument pd : dp) {

						doc = new Document();

						// add the document identifier
						doc.add(new StringField(ParsedDocument.FIELDS.ID, pd.getIdentifier(), Field.Store.YES));

						// add the document body
						doc.add(new BodyField(pd.getBody()));

						writer.addDocument(doc);

						docsCount++;

						// print progress every 10000 indexed documents
						if (docsCount % 10000 == 0) {
							System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n",
									docsCount, filesCount, bytesCount / MBYTE,
									(System.currentTimeMillis() - start) / 1000);
						}

					}

				}
				return FileVisitResult.CONTINUE;
			}
		});

		writer.commit();

		writer.close();

		if (docsCount != expectedDocs) {
			System.out.printf("Expected to index %d documents; %d indexed instead.%n", expectedDocs, docsCount);
		}

		System.out.printf("%d document(s) (%d files, %d Mbytes) indexed in %d seconds.%n", docsCount, filesCount,
				bytesCount / MBYTE, (System.currentTimeMillis() - start) / 1000);

		System.out.printf("#### Indexing complete ####%n");
	}

	/**
	 * Prints statistics about the vocabulary to a file.
	 *
	 * @throws IOException if something goes wrong while accessing the index.
	 */
	public void printVocabularyStatistics() throws IOException {

		System.out.printf("%n------------- PRINTING VOCABULARY STATISTICS -------------%n");

		// Keep statistics about the vocabulary. Each key is a term; each value is an array where the first element is
		// the total frequency of the term and the second element is the document frequency.
		final Map<String, long[]> stats = new HashMap<>(2000000);

		// create a new vocabulary file, replacing the existing one, if any
		final PrintWriter out = new PrintWriter(Files.newBufferedWriter(vocabularyPath, StandardCharsets.UTF_8, StandardOpenOption.CREATE,
				StandardOpenOption.TRUNCATE_EXISTING, StandardOpenOption.WRITE));

		// Open the directory in Lucene
		final Directory dir = FSDirectory.open(indexDir);

		// Get a reader for the index
		final IndexReader index = DirectoryReader.open(dir);

		// total number of documents
		long totDocs = 0;

		// total number of terms
		long totTerms = 0;

		// get the leaf readers, i.e. the readers for each segment of the index
		for(LeafReaderContext lrc : index.leaves()) {

			LeafReader lidx = lrc.reader();

			// Increment the total number of documents
			totDocs += lidx.numDocs();

			// Get the vocabulary of this leaf index.
			Terms voc = lidx.terms(ParsedDocument.FIELDS.BODY);

			// Get an iterator over the vector of terms in the vocabulary
			TermsEnum termsEnum = voc.iterator();

			// Iterate until there are terms
			for (BytesRef term = termsEnum.next(); term != null; term = termsEnum.next()) {

				// Get the text string of the term
				String termstr = term.utf8ToString();

				// Get the total frequency of the term
				long freq = termsEnum.totalTermFreq();

				// Increment the total number of terms
				totTerms += freq;

				// Get the document frequency (DF) of the term
				int df = termsEnum.docFreq();

				// update the statistics with the new entry
				stats.compute(termstr, (k, v) -> {

					// if the term is not already in the statistics, create a new pair for its counts
					if (v == null) {
						v = new long[2];
					}

					// Update the counts
					v[0] += freq;
					v[1] += df;

					// return the updated counts
					return v;
				});
			}

		}

		// close index and directory
		index.close();
		dir.close();

		System.out.printf("+ Total number of documents %d%n", totDocs);

		System.out.printf("+ Total number of unique terms: %d%n", stats.size());

		System.out.printf("+ Total number of terms: %d%n", totTerms);

		// Print vocabulary statistics to the output file
		System.out.printf("+ Printing vocabulary statistics to %s%n", vocabularyPath.toAbsolutePath().toString());
		out.printf("%s\t%s\t%s%n", "Term", "RawFrequency", "DocumentFrequency");

		stats.forEach( (k, v) -> {
			out.printf("%s\t%d\t%d%n", k, v[0], v[1]);
		});

		// close vocabulary file
		out.flush();
		out.close();

		System.out.printf("----------------------------------------------------------%n");
	}


	/**
	 * Main method of the class. Just for testing purposes.
	 *
	 * @param args command line arguments.
	 *
	 * @throws Exception if something goes wrong while indexing.
	 */
	public static void main(String[] args) throws Exception {

		final int ramBuffer = 256;
		final String docsPath = "/Users/ferro/Documents/experimental-collections/corpora/TREC/tipster_trec07";
		final String indexPath = "experiment/index-nostop-nostem";
		final String vocabularyFile = "experiment/vocabulary.txt";

		final String extension = "txt";
		final int expectedDocs = 528155;
		final String charsetName = "ISO-8859-1";

		final Analyzer a = CustomAnalyzer.builder().withTokenizer(StandardTokenizerFactory.class).addTokenFilter(
				LowerCaseFilterFactory.class).build();

		DirectoryIndexer i = new DirectoryIndexer(a, ramBuffer, indexPath, docsPath, extension,
				charsetName, expectedDocs, TipsterParser.class, vocabularyFile);

		i.index();

		i.printVocabularyStatistics();

	}

}
