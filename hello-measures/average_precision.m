%
% Copyright 2021-2022 University of Padua, Italy
%
% Licensed under the Apache License, Version 2.0 (the "License");
% you may not use this file except in compliance with the License.
% You may obtain a copy of the License at
%
%     http://www.apache.org/licenses/LICENSE-2.0
%
% Unless required by applicable law or agreed to in writing, software
% distributed under the License is distributed on an "AS IS" BASIS,
% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
% See the License for the specific language governing permissions and
% limitations under the License.
%
% Author: Nicola Ferro (ferro@dei.unipd.it)


% A topic of a run. 
% Each position is a retrieved document; first element of the vector is 
% the top of the rank. 
% Assume binary relevance: 0 for not relevant; 1 for relevant.
run = [0 1 1 0 1 0 0 1 1 0];

% Recall Base
RB = 5;

% Find the rank positions of the relevant retrieved documents
ranks = find(run);

% compute average precision, if any relevant document has been retrieved
if ~isempty(ranks)
    ap = sum(cumsum(run(ranks)) ./ ranks) ./ RB;
else
    ap = 0;
end
