/*
 * Copyright 2021-2022 University of Padua, Italy
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package it.unipd.dei.se;


import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.codecs.Codec;
import org.apache.lucene.codecs.lucene99.Lucene99Codec;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

import org.apache.lucene.codecs.simpletext.*;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

/**
 * Introductory example on how to use <a href="https://lucene.apache.org/" target="_blank">Apache Lucene</a> to index
 * and search for toy documents.
 *
 * @author Nicola Ferro (ferro@dei.unipd.it)
 * @version 1.0
 * @since 1.0
 */
public class HelloIndex {

	/**
	 * The first toy document.
	 * <p>
	 * Credits to Maria Maistro.
	 */
	private static final String D1 =
			"The quokka, the only member of the genus Setonix, is a small marsupial about the size of a domestic cat";

	/**
	 * The second toy document.
	 * <p>
	 * Credits to Maria Maistro.
	 */
	private static final String D2 =
			"Wombats are small, short-legged, muscular quadrupedal marsupials that are native to Australia";

	/**
	 * The third toy document.
	 * <p>
	 * Credits to Maria Maistro.
	 */
	private static final String D3 =
			"Quokkas have little fear of humans and commonly approach people closely, particularly on Rottnest Island in Australia, where a prevalent population exists";

	/**
	 * The name of the {@code id} {@link org.apache.lucene.document.Field} in a {@link Document}.
	 */
	static final String ID = "id";

	/**
	 * The name of the {@code body} {@link org.apache.lucene.document.Field} in a {@link Document}.
	 */
	static final String BODY = "body";

	/**
	 * The default path to the directory containing the index, if none is provided.
	 */
	private static final String DEFAULT_INDEX_PATH = "experiment/index";

	/**
	 * The path to the directory containing the index.
	 */
	private final Path indexPath;


	/**
	 * Creates a new instance for the specified index directory.
	 *
	 * @param indexPath the path where to store the index.
	 *
	 * @throws IOException if something goes wrong accessing the index directory.
	 */
	public HelloIndex(String indexPath) throws IOException {

		System.out.printf("%n%n############ HELLO, Indexer! ############%n%n");

		// check whether we have a path for the index or we should use the default one
		if (indexPath == null || indexPath.isEmpty()) {
			indexPath = DEFAULT_INDEX_PATH;
		}

		// get access to the file system
		final FileSystem fs = FileSystems.getDefault();

		// get the path where to store the index
		this.indexPath = fs.getPath(indexPath);

		// if the directory does not already exist, create it
		if (Files.notExists(this.indexPath)) {
			Files.createDirectory(this.indexPath);
		}

		// check that the path is a directory (only needed if it already existed, always true if we created it)
		if (!Files.isDirectory(this.indexPath)) {
			throw new IllegalStateException(
					String.format("%s is not a directory.", this.indexPath.toAbsolutePath().toString()));
		}

		System.out.printf("%n------------- INITIALIZING -------------%n");
		System.out.printf("- Index stored at: %s%n", this.indexPath.toAbsolutePath().toString());
		System.out.printf("----------------------------------------%n");
	}

	/**
	 * Creates a new instance using the default path for the index directory .
	 *
	 * @throws IOException if something goes wrong accessing the index directory.
	 */
	public HelloIndex() throws IOException {
		this(null);
	}

	/**
	 * Parses the toy documents and returns a list of {@link Document}s.
	 *
	 * @return the list of documents.
	 */
	public List<Document> parseDocuments() {

		System.out.printf("%n------------- PARSING DOCUMENTS -------------%n");

		// the list of documents
		final ArrayList<Document> docs = new ArrayList<>();

		Document d;

		// create a new document for D1
		d = new Document();
		d.add(new StringField(ID, "d1", Field.Store.YES));
		d.add(new BodyField(D1));
		docs.add(d);

		// create a new document for D2
		d = new Document();
		d.add(new StringField(ID, "d2", Field.Store.YES));
		d.add(new BodyField(D2));
		docs.add(d);

		// create a new document for D3
		d = new Document();
		d.add(new StringField(ID, "d3", Field.Store.YES));
		d.add(new BodyField(D3));
		docs.add(d);

		System.out.printf("The documents are:%n");
		docs.forEach(System.out::println);
		System.out.printf("---------------------------------------------%n");

		return docs;
	}

	/**
	 * Indexes the provided {@link Document}s with the given {@link Analyzer}.
	 *
	 * @param docs     the documents to be indexed.
	 * @param analyzer the analyzer to be used.
	 *
	 * @throws IOException if something goes wrong while indexing.
	 */
	public void index(final List<Document> docs, final Analyzer analyzer) throws IOException {

		System.out.printf("%n------------- INDEXING DOCUMENTS -------------%n");

		// Open the directory in Lucene
		final Directory directory = FSDirectory.open(indexPath);

		// Utility class for holding all the required configuration for the indexer
		final IndexWriterConfig config = new IndexWriterConfig(analyzer);

		// force to re-create the index if it already exists
		config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);

		// set the similarity. BM25 is already the default one
		config.setSimilarity(new BM25Similarity());

		// set the text-based codec
		//config.setCodec(new Lucene99Codec(Lucene99Codec.Mode.BEST_SPEED));

		config.setCodec(new SimpleTextCodec());

		// prevent using the compound file format
		config.setUseCompoundFile(false);

		// The actual indexer
		final IndexWriter writer = new IndexWriter(directory, config);

		System.out.printf("- Indexer successfully created%n");

		for (Document d : docs) {
			writer.addDocument(d);

			System.out.printf("- Document %s successfully indexed%n", d.get(ID));
		}

		writer.close();
		directory.close();
		System.out.printf("- Indexer successfully closed%n");

		System.out.printf("----------------------------------------------%n");

	}

	/**
	 * Prints the inverted index to the console.
	 *
	 * @throws IOException if something goes wrong while accessing the index.
	 */
	public void printInvertedIndex() throws IOException {

		System.out.printf("%n------------- PRINTING THE INVERTED INDEX -------------%n");

		// Open the directory in Lucene
		final Directory dir = FSDirectory.open(indexPath);

		// Open the index
		final IndexReader index = DirectoryReader.open(dir);

		// The inverted index. Keys are terms; values are lists of (docID, term frequency) pairs
		final Map<String, List<Map.Entry<String, Long>>> invertedIndex = new TreeMap<>();

		// Iterate over each document in the index
		final StoredFields storedFields = index.storedFields();
		final TermVectors termVectors = index.termVectors();

		for (int i = 0, docs = index.numDocs(); i < docs; i++) {

			// Read the document and get its identifier
			String docID = storedFields.document(i).get(ID);

			// Get the vector of terms for that document
			Terms terms = termVectors.get(i).terms(BODY);

			// Get an iterator over the vector of terms
			TermsEnum termsEnum = terms.iterator();

			// Iterate until there are terms
			for (BytesRef term = termsEnum.next(); term != null; term = termsEnum.next()) {

				// Get the text string of the term
				String termstr = term.utf8ToString();

				// Get the total frequency of the term
				long freq = termsEnum.totalTermFreq();

				// Create a new (docID, term frequency) pair
				Map.Entry<String, Long> entry = new AbstractMap.SimpleEntry<>(docID, freq);

				// update the inverted index with the new entry
				invertedIndex.compute(termstr, (k, v) -> {

					// if the term is not already in the index, create a new list for its pairs
					if (v==null) {
						v = new ArrayList<>();
					}

					// add the pair to the list
					v.add(entry);

					// return the updated list
					return v;
				});
			}
		}

		// close the index and the directory
		index.close();
		dir.close();

		// Print the inverted index to the console
		invertedIndex.forEach( (k, v) -> {

			System.out.printf("+ %s ->", k);

			v.forEach((e) -> {
				System.out.printf(" %s:%d", e.getKey(), e.getValue());
			});

			System.out.printf("%n");

		});

		System.out.printf("-------------------------------------------------------%n");
	}

	/**
	 * Prints statistics about the vocaulary to the console.
	 *
	 * @throws IOException if something goes wrong while accessing the index.
	 */
	public void printVocabularyStatistics() throws IOException {

		System.out.printf("%n------------- PRINTING VOCABULARY STATISTICS -------------%n");

		// Open the directory in Lucene
		final Directory dir = FSDirectory.open(indexPath);

		// Open the index - we need to get a LeafReader to be able to directly access terms
		final LeafReader index = DirectoryReader.open(dir).leaves().get(0).reader();

		// Total number of documents in the collection
		System.out.printf("+ Total number of documents: %d%n", index.numDocs());

		// Get the vocabulary of the index.
		final Terms voc = index.terms(BODY);

		// Total number of unique terms in the collection
		System.out.printf("+ Total number of unique terms: %d%n", voc.size());

		// Total number of terms in the collection
		System.out.printf("+ Total number of terms: %d%n", voc.getSumTotalTermFreq());

		// Get an iterator over the vector of terms in the vocabulary
		final TermsEnum termsEnum = voc.iterator();

		// Iterate until there are terms
		System.out.printf("+ Vocabulary:%n");
		System.out.printf("  - %-20s%-5s%-5s%n", "TERM", "DF", "FREQ");
		for (BytesRef term = termsEnum.next(); term != null; term = termsEnum.next()) {

			// Get the text string of the term
			String termstr = term.utf8ToString();

			// Get the document frequency (DF) of the term
			int df = termsEnum.docFreq();

			// Get the total frequency of the term
			long freq = termsEnum.totalTermFreq();
			System.out.printf("  - %-20s%-5d%-5d%n", termstr, df, freq);
		}

		// close the index and the directory
		index.close();
		dir.close();

		System.out.printf("----------------------------------------------------------%n");
	}

	/**
	 * Main method of the class.
	 *
	 * @param args command line arguments. If provided, {@code args[0]} contains the path the the index directory.
	 *
	 * @throws IOException if something goes wrong while indexing.
	 */
	public static void main(String[] args) throws IOException {

		final HelloIndex hidx;

		if (args.length == 1) {
			hidx = new HelloIndex(args[0]);
		} else {
			hidx = new HelloIndex();
		}

		// create the list of documents from the provided toy ones
		final List<Document> docs = hidx.parseDocuments();

		// The analyzer to be used for document pre-processing
		// The StandardAnalyzer tokenizes documents using spaces and recognizing, e.g., urls; then it turns tokens
		// lower-case and removes stop words.
		final Analyzer analyzer = new StandardAnalyzer();

		// index the documents
		hidx.index(docs, analyzer);

		// print the inverted index to the console
		hidx.printInvertedIndex();

		// print vocabulary statistics to the console
		hidx.printVocabularyStatistics();

	}

}
