%% common_parameters
% 
% Sets up parameters common to the different scripts.
%
%% Information
% 
% * *Author*: <mailto:ferro@dei.unipd.it Nicola Ferro>
% * *Version*: 1.00
% * *Since*: 1.00
% * *Requirements*: MATTERS 1.0 or higher; Matlab 2017b
% * *Copyright:* (C) 2021 <http://www.dei.unipd.it/ 
% Department of Information Engineering> (DEI), <http://www.unipd.it/ 
% University of Padua>, Italy
% * *License:* <http://www.apache.org/licenses/LICENSE-2.0 Apache License, 
% Version 2.0>

%%
%{
 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at
 
      http://www.apache.org/licenses/LICENSE-2.0
 
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
%}

diary off;

%% Path Configuration
EXPERIMENT.path.base = '/Users/ferro/Documents/progetti/software/search-engines/se-unipd/hello-matters/data/';

% The path for the datasets, i.e. the runs and the pools
EXPERIMENT.path.dataset = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'dataset', filesep);

% The path for the measures
EXPERIMENT.path.measure = sprintf('%1$s%2$s%3$s', EXPERIMENT.path.base, 'measure', filesep);


%% General Configuration

% Label of the paper this experiment is for
EXPERIMENT.label.paper = 'SEARCH ENGINES';


%% Configuration for Tracks

EXPERIMENT.track.list = {'T08'};
EXPERIMENT.track.number = length(EXPERIMENT.track.list);

% TREC 08, 1999, Adhoc
EXPERIMENT.track.T08.id = 'T08';
EXPERIMENT.track.T08.name = 'TREC 08, 1999, Adhoc';
EXPERIMENT.track.T08.runs = 129;
EXPERIMENT.track.T08.topics = 50;
EXPERIMENT.track.T08.runLength = 1000;
EXPERIMENT.track.T08.pool.file = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/pool/qrels.trec8.adhoc.txt';
EXPERIMENT.track.T08.pool.relevanceDegrees = {'NotRelevant', 'Relevant'};
EXPERIMENT.track.T08.pool.relevanceGrades = 0:1;
EXPERIMENT.track.T08.pool.delimiter = 'space';
EXPERIMENT.track.T08.pool.import = @(fileName, id) importPoolFromFileTRECFormat('FileName', fileName, 'Identifier', id, 'RelevanceDegrees', EXPERIMENT.track.T08.pool.relevanceDegrees, 'RelevanceGrades', EXPERIMENT.track.T08.pool.relevanceGrades, 'Delimiter', EXPERIMENT.track.T08.pool.delimiter,  'Verbose', false);
EXPERIMENT.track.T08.run.path = '/Users/ferro/Documents/experimental-collections/TREC/TREC_08_1999_AdHoc/runs/all';
EXPERIMENT.track.T08.run.documentOrdering = 'TrecEvalLexDesc';
EXPERIMENT.track.T08.run.singlePrecision = true;
EXPERIMENT.track.T08.run.delimiter = 'tab';
EXPERIMENT.track.T08.run.import = @(runPath, id, requiredTopics) importRunsFromDirectoryTRECFormat('Path', runPath, 'Identifier', id, 'RequiredTopics', requiredTopics, 'DocumentOrdering', EXPERIMENT.track.T08.run.documentOrdering, 'SinglePrecision', EXPERIMENT.track.T08.run.singlePrecision, 'Delimiter', EXPERIMENT.track.T08.run.delimiter, 'Verbose', false);


% Returns the name of a track given its index in EXPERIMENT.track.list
EXPERIMENT.track.getName = @(idx) ( EXPERIMENT.track.(EXPERIMENT.track.list{idx}).name ); 



%% Patterns for file names

% ALL - Pattern <path>/<trackID>/<fileID>.<ext>
EXPERIMENT.pattern.file.general.track = @(path, trackID, fileID, ext) sprintf('%1$s%2$s%3$s%4$s.%5$s', path, trackID, filesep, fileID, ext);

% MAT - Pattern EXPERIMENT.path.base/dataset/<trackID>/<datasetID>.mat
EXPERIMENT.pattern.file.dataset = @(trackID, datasetID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.dataset, trackID, datasetID, 'mat');

% MAT - Pattern EXPERIMENT.path.base/measure/<trackID>/<measureID>.mat
EXPERIMENT.pattern.file.measure = @(trackID, measureID) EXPERIMENT.pattern.file.general.track(EXPERIMENT.path.measure, trackID, measureID, 'mat');


%% Patterns for identifiers

% Pattern pool_<trackID>
EXPERIMENT.pattern.identifier.pool =  @(trackID) sprintf('pool_%1$s', trackID);

% Pattern run_<trackID>
EXPERIMENT.pattern.identifier.run = @(trackID) sprintf('run_%1$s', trackID);

% Pattern <measureID>_<trackID>
EXPERIMENT.pattern.identifier.measure =  @(measureID, trackID) sprintf('%1$s_%2$s', measureID, trackID);


%% Configuration for measures

% The list of measures under experimentation
EXPERIMENT.measure.list = {'ap', 'p5', 'rprec', 'rbp', 'dcg', 'ndcg', 'ndcg20'};
EXPERIMENT.measure.number = length(EXPERIMENT.measure.list);

% Configuration for AP
EXPERIMENT.measure.ap.id = 'ap';
EXPERIMENT.measure.ap.acronym = 'AP';
EXPERIMENT.measure.ap.name = 'Average Precision';
EXPERIMENT.measure.ap.compute = @(pool, runSet) averagePrecision(pool, runSet);

% Configuration for P@5
EXPERIMENT.measure.p5.id = 'p5';
EXPERIMENT.measure.p5.acronym = 'P@5';
EXPERIMENT.measure.p5.name = 'Precision at 5 Retrieved Documents';
EXPERIMENT.measure.p5.cutoffs = 5;
EXPERIMENT.measure.p5.compute = @(pool, runSet) precision(pool, runSet, 'Cutoffs', EXPERIMENT.measure.p5.cutoffs);

% Configuration for R-prec
EXPERIMENT.measure.rprec.id = 'rprec';
EXPERIMENT.measure.rprec.acronym = 'R-prec';
EXPERIMENT.measure.rprec.name = 'Precision at the Recall Base';
EXPERIMENT.measure.rprec.compute = @(pool, runSet) precision(pool, runSet, 'Rprec', true);

% Configuration for RBP
EXPERIMENT.measure.rbp.id = 'rbp';
EXPERIMENT.measure.rbp.acronym = 'RBP';
EXPERIMENT.measure.rbp.name = 'Rank-biased Precision';
EXPERIMENT.measure.rbp.persistence = 0.8;
EXPERIMENT.measure.rbp.compute = @(pool, runSet) rankBiasedPrecision(pool, runSet, 'Persistence', EXPERIMENT.measure.rbp.persistence);

% Configuration for DCG
EXPERIMENT.measure.dcg.id = 'dcg';
EXPERIMENT.measure.dcg.acronym = 'DCG';
EXPERIMENT.measure.dcg.name = 'Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.dcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.dcg.logBase = 10;
EXPERIMENT.measure.dcg.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.dcg.compute = @(pool, runSet) discountedCumulatedGain(pool, runSet, 'CutOffs', EXPERIMENT.measure.dcg.cutoffs, 'LogBase', EXPERIMENT.measure.dcg.logBase, 'Normalize', false, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.dcg.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.dcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG
EXPERIMENT.measure.ndcg.id = 'ndcg';
EXPERIMENT.measure.ndcg.acronym = 'nDCG';
EXPERIMENT.measure.ndcg.name = 'Normalized Discounted Cumulated Gain at Last Retrieved Document';
EXPERIMENT.measure.ndcg.cutoffs = 'LastRelevantRetrieved';
EXPERIMENT.measure.ndcg.logBase = 10;
EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg.compute = @(pool, runSet) discountedCumulatedGain(pool, runSet, 'CutOffs', EXPERIMENT.measure.ndcg.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg.fixedNumberRetrievedDocumentsPaddingStrategy);

% Configuration for nDCG@20
EXPERIMENT.measure.ndcg20.id = 'ndcg20';
EXPERIMENT.measure.ndcg20.acronym = 'nDCG@20';
EXPERIMENT.measure.ndcg20.name = 'Normalized Discounted Cumulated Gain at 20 Retrieved Documents';
EXPERIMENT.measure.ndcg20.cutoffs = 20;
EXPERIMENT.measure.ndcg20.logBase = 10;
EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments = 1000;
EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy = 'NotRelevant';
EXPERIMENT.measure.ndcg20.compute = @(pool, runSet) discountedCumulatedGain(pool, runSet, 'CutOffs', EXPERIMENT.measure.ndcg20.cutoffs, 'LogBase', EXPERIMENT.measure.ndcg20.logBase, 'Normalize', true, 'FixNumberRetrievedDocuments', EXPERIMENT.measure.ndcg20.fixNumberRetrievedDocuments, 'FixedNumberRetrievedDocumentsPaddingStrategy', EXPERIMENT.measure.ndcg20.fixedNumberRetrievedDocumentsPaddingStrategy);


% Returns the identifier of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getID = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).id ); 

% Returns the acronym of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getAcronym = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).acronym ); 

% Returns the name of a measure given its index in EXPERIMENT.measure.list
EXPERIMENT.measure.getName = @(idx) ( EXPERIMENT.measure.(EXPERIMENT.measure.list{idx}).name ); 

